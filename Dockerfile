FROM node:carbon

WORKDIR /app
COPY package.json *.js ./

RUN apt-get update -qq \
    && apt-get install -qq \
        build-essential python \
        bluetooth bluez libbluetooth-dev libudev-dev \
    && yarn

CMD ["yarn", "start"]
