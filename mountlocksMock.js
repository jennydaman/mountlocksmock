/*
 * BLE service with two characteristics, dubbed 'rx' and 'tx'.
 * 'tx' is read-only and will transmit a predefined string.
 * 'rx' prints incoming data to console output, encoded as a string.
 */

const bleno = require('bleno');
const Characteristic = bleno.Characteristic;

// ========================================
// Definitions
// ========================================

const name = 'Mount Locks (mock)';
const serviceUuid = 'ace6e3e345494b5c86116ca6265e340a';
const uuidTx = '2263';
const uuidRx = '4B66';
const txStr = 'hello how are you';
const txBuffer = Buffer.from(txStr);


const charTx = new Characteristic({
  uuid: uuidTx,
  properties: [ 'read' ],
  value: Buffer.from(txStr),
  descriptors: [
    new bleno.Descriptor({uuid: uuidTx, value: 'Mount Locks TX transmission'})
  ],
  onReadRequest: (offset, callback) => {
    print('charTx.onReadRequest', 'read');
    console.log('---> ' + txStr);
    callback(Characteristic.RESULT_SUCCESS, txBuffer);
  },
  onWriteRequest: (data, offset, withoutResponse, callback) => {
    print('charTx.onWriteRequest', 'write');
    console.log('<--- ' + data.toString());
    callback(Characteristic.RESULT_UNLIKELY_ERROR);
  },
  onSubscribe: (maxValueSize, updateValueCallback) => {
    print('charTx.onSubscribe', 'maxValueSize=' + maxValueSize);
    updateValueCallback();
  },
  onUnsubscribe: () => print('charTx.onUnsubscribe'),
  onNotify: () => print('charTx.onNotify'),
  onIndicate: () => print('charTx.onIndicate')
});

const charRx = new Characteristic({
  uuid: uuidRx,
  properties: [ 'write' ],
  descriptors: [
    new bleno.Descriptor({uuid: uuidRx, value: 'Mount Locks RX reception'})
  ],
  onReadRequest: (offset, callback) => {
    print('charRx.onReadRequest', 'read');
    const youreWrong = 'should not be reading from rx characteristic';
    console.log('---> ' + youreWrong);
    callback(Characteristic.RESULT_UNLIKELY_ERROR, Buffer.from(youreWrong));
  },
  onWriteRequest: (data, offset, withoutResponse, callback) => {
    print('charRx.onWriteRequest', 'write');
    console.log('<--- ' + data.toString());
    callback(Characteristic.RESULT_SUCCESS);
  },
  onSubscribe: (maxValueSize, updateValueCallback) => {
    print('charRx.onSubscribe', 'maxValueSize=' + maxValueSize);
    updateValueCallback();
  },
  onUnsubscribe: () => print('charRx.onUnsubscribe'),
  onNotify: () => print('charRx.onNotify'),
  onIndicate: () => print('charRx.onIndicate')
});

// ========================================
// Setup
// ========================================

print('start');

bleno.on('stateChange', state => {
  print('bleno.on:stateChange', state);
  if (state === 'poweredOn') {
    bleno.startAdvertising(name, [serviceUuid], error => throwError(error, 'bleno.on:poweredOn'));
  } else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', error => {
  print('bleno.on:advertisingStart');
  throwError(error);
  bleno.setServices([
    new bleno.PrimaryService({
      uuid: serviceUuid,
      characteristics: [ charTx, charRx ]
    })
  ], error => throwError(error, 'bleno.setServices'));
});

// ========================================
// Helper functions
// ========================================

/**
 * Print a timestamp and a message.
 *
 * @param func calling function
 * @param message
 */
function print(func, message) {
  console.log(`[$${Date.now()}] (${func}) ${message || ''}`);
}

function throwError(error, func) {
  if (!error) {
    return;
  }
  if (func) {
    print(func, 'error');
  }
  throw error;
}
