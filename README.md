# Mount Locks Mock Device

Simple _bleno_ example.

# Build

```bash
docker build -t mountlocksmock .
```

# Example

```bash
docker run --privileged --net=host mountlocksmock
```

## Input

![screenshot](screenshot.jpg)

## Output

```
yarn run v1.12.3
$ node mountlocksMock.js
[$1594559621277] (start)
[$1594559621402] (bleno.on:stateChange) poweredOn
[$1594559621406] (bleno.on:advertisingStart)
[$1594559658774] (charRx.onWriteRequest) write
<--- hello can you hear m
[$1594559658834] (charRx.onWriteRequest) write
<--- e
```
